
## Apache Druid
${DRUID_INSTALL_DIR}/bin/start-micro-quickstart
Open: http://localhost:8888
Optional: 
Change property 'druid.worker.capacity' to '5' in file:
${DRUID_INSTALL_DIR}/conf/druid/single-server/micro-quickstart/middleManager

## Apache Kafka
${KAFKA_INSTALL_DIR}/bin/kafka-server-start.sh ${KAFKA_INSTALL_DIR}/config/server.properties

## Telegraph
telegraf --config ./config/telegraf/telegraf-kafka.conf

## Turnilo
Run: turnilo --druid http://localhost:8888
Open: http://localhost:9090



